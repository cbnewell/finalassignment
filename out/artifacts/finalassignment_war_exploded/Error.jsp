<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/15/2020
  Time: 12:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error</title>
</head>
<body>
<h1>Error</h1>
<h2><%= exception.getMessage() %></h2>
</body>
</html>

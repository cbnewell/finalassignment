<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/15/2020
  Time: 1:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<html>
<head>
    <title>Library Item Form</title>
</head>
<body>
    <br>
    <div class="container col-md-5">
        <div>
            <div class="body">



                    <input type="hidden" name="ItemID" value="${item.ItemID}"/>


                <fieldset>
                    <label>Item Name</label>
                    <input type="text" value="${item.ItemName}" class="form-control" name="ItemName" required="required"/>
                </fieldset>
                <fieldset>
                    <label>Item Type</label>
                    <input type="text" value="${item.ItemType}" class="form-control" name="ItemType" required="required"/>
                </fieldset>
                <fieldset>
                    <label>Item Available</label>
                    <input type="text" value="${item.ItemAvail}" class="form-control" name="ItemAvail" required="required"/>
                </fieldset>

                <button type="submit" class="btn btn-success">Save</button>

                    </form>
            </div>
        </div>
    </div>
</body>
</html>


<%--
  Created by IntelliJ IDEA.
  User: Brycen Newell
  Date: 12/11/2020
  Time: 11:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        head {
            justify-content: normal;
        }
        body {
            text-align: center;
            display: flex;
            justify-content: center;
            background-color: lightgray;
        }
        label {
            text-align: left;
        }
        input[type=text], select, input[type=password], input[type=date] {
            text-align: left;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 2px;
            border: 1px solid #000000;
            border-radius: 2px;
            box-sizing: border-box;
        }
        input[type=submit] {

            width: 100%;
            background-color: navy;
            color: whitesmoke;
            padding: 12px 20px;
            margin: 8px 2px;
            border: none;
            border-radius: 2px;

        }
        div {
            margin: 0 auto;
            width: 50%;
            border-radius: 2px;
            background-color: whitesmoke;
            padding: 12px 20px;
        }
    </style>

    <title> Library Item List </title>
</head>


<body>
    <div >
        <div >
            <h3>List of Users</h3>
            <hr>
            <div>
                <a href="<%=request.getContextPath()%>/item-form.jsp" >Add New Item to Library</a>
            </div>
            <br>
        </div>
        <table >
            <thead>
                <tr>
                    <th>Item ID</th>
                    <th>Item Name</th>
                    <th>Item Type</th>
                    <th>Item Available</th>
                </tr>
            </thead>
            <tbody>


                    <tr>
                        <td>${item.ItemID}</td>
                        <td>${item.ItemName}</td>
                        <td>${item.ItemType} </td>
                        <td>${item.ItemAvail}</td>
                        <td>
                            <a href=edit?id=${item.ItemID}>Edit Item</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href=delete?id=${item.ItemID}>Delete Item</a>
                        </td>
                    </tr>

            </tbody>
        </table>
    </div>

</body>


</html>
package ResponseFiles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class loginResponse {

    public static String returnLoginResponse(String Username, String Password) {



        String loginResponseRecorded = "You have been successfully logged in";
        String failedLoginResponse = "Sorry, it appears you may have typed something incorrectly.";
        String htmlLoginResponse = "<html><head><title>User Authentication Success</title></head> <body><h1>" + loginResponseRecorded + "</h1></body></html>";


            if (!Authentication(Username, Password)) {


                return htmlLoginResponse;
                // then should direct to proper viewItem Servlet for next step
            } else {
                return "<html><head><title>User Authentication FailedAut</title></head> <body><h1>" + failedLoginResponse + "</h1></body></html>";
            }


    }

    public static boolean Authentication(String Username, String Password) {
        int maxLength = 30;

        Pattern usernamePattern = Pattern.compile("[^a-zA-Z0-9]");
        Pattern passwordPattern = Pattern.compile("[^a-zA-Z0-9_#$%&’*+/=?^.-]");

        if((Username != null && Username.length() <= maxLength) && (Password != null && Password.length() <= maxLength)) {

            Matcher userMatcher = usernamePattern.matcher(Username);
            Matcher passMatcher = passwordPattern.matcher(Password);
            boolean userMatch = userMatcher.find();
            boolean passMatch = passMatcher.find();

            if (!userMatch || passMatch) {
                return false;

            }
        } else {
            return false;
        }
        return true;
    }





}

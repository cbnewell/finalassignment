package HibernateFiles;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ItemDAO {

    private String jdbcURL = "jdbc:mysql://localhost:3306/librarydatabase?useSSL=false";
    private String jdbcUsername = "root";
    private String jdbcPassword = "Str@wb3rry!";
    private String jdbcDriver = "com.mysql.jdbc.Driver";

    private static final String INSERT_ITEMS_SQL = "INSERT INTO itemDB" + " (ItemName, ItemType, ItemAvail) VALUES " + " (?, ?, ?);";

    private static final String SELECT_ITEM_BY_ITEMID = "select ItemID, ItemName, ItemType, ItemAvail from itemDB where ItemID=?";
    private static final String SELECT_ALL_ITEMS = "select * from itemDB";
    private static final String DELETE_ITEMS = "delete from itemDB where ItemID = ?;";
    private static final String UPDATE_ITEMS = "update itemDB set ItemName = ?, ItemType = ?, ItemAvail = ? where ItemID = ?;";


    public ItemDAO() {

    }

    // establish connection to database
    protected Connection getConnection(String jdbcDriver, String jdbcURL, String jdbcUsername, String jdbcPassword) {
        return getConnection(this.jdbcDriver, this.jdbcURL, this.jdbcUsername, this.jdbcPassword);

    }

    /* CRUD operations */

    // insert items
    public void insertItems(Item item) throws SQLException {
        System.out.println(INSERT_ITEMS_SQL);
        try (Connection connection = getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ITEMS_SQL)) {
            preparedStatement.setString(1, item.getItemName());
            preparedStatement.setString(2, item.getItemType());
            preparedStatement.setString(3, item.getItemAvail());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }



    // select item by item ID
    public Item selectItem(int ItemID) {
        Item item = null;
        // Establish connection
        try (Connection connection = getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);
             //Create a statement using connection
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ITEM_BY_ITEMID);) {
            preparedStatement.setInt(1, ItemID);
            System.out.println(preparedStatement);
            // Execute query or update the query
            ResultSet rs = preparedStatement.executeQuery();

            // process ResultSet
            while (rs.next()) {
                String ItemName = rs.getString("ItemName");
                String ItemType = rs.getString("ItemType");
                String ItemAvail = rs.getString("ItemAvail");
                item = new Item(ItemID, ItemName, ItemType, ItemAvail);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return item;
    }

    // select all items
    public List<Item> selectAllItems() {
        List<Item> items = new ArrayList<>();

        try (Connection connection = getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_ITEMS);) {

            System.out.println(preparedStatement);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int ItemID = rs.getInt("ItemID");
                String ItemName = rs.getString("ItemName");
                String ItemType = rs.getString("ItemType");
                String ItemAvail = rs.getString("ItemAvail");
                items.add(new Item(ItemID, ItemName, ItemType, ItemAvail));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return items;
    }

    // update item
    public boolean updateItem(Item item) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ITEMS);)    {
            System.out.println("updated item:" + preparedStatement);
            preparedStatement.setString(1, item.getItemName());
            preparedStatement.setString(2, item.getItemType());
            preparedStatement.setString(3, item.getItemAvail());
            preparedStatement.setInt(4, item.getItemID());

            rowUpdated = preparedStatement.executeUpdate() > 0;

        }
        return rowUpdated;
    }

    // delete all items
    public boolean deleteItem(int ItemID) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ITEMS);) {
            preparedStatement.setInt(1, ItemID);
            rowDeleted = preparedStatement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    // created exception to get better thrown error
    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof  SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


}


package HibernateFiles;


import org.jetbrains.annotations.Nullable;

import java.sql.*;


public class LoginDAO {

    private String jdbcURL = "jdbc:mysql://localhost:3306/librarydatabase?useSSL=false";
    private String jdbcUsername = "root";
    private String jdbcPassword = "Str@wb3rry!";
    private String jdbcDriver = "com.mysql.jdbc.Driver";

    private static final String SELECT_USER_BY_USERNAME = "select Username, Password from userDB where Username=?";


    public LoginDAO() {

    }

    // establish connection to database
    protected Connection getConnection() {
        return getConnection(jdbcDriver, jdbcURL, jdbcUsername, jdbcPassword);

    }

    @Nullable
    static Connection getConnection(String jdbcDriver, String jdbcURL, String jdbcUsername, String jdbcPassword) {
        Connection connection = null;

        try {
            Class.forName(jdbcDriver);
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /* CRUD operations */




    // select user by user ID
    public userLogin selectUserByName(String Username) {
        userLogin user = null;
        // Establish connection
        try (Connection connection = getConnection();
             //Create a statement using connection
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_USERNAME)) {
            preparedStatement.setString(1, Username);
            System.out.println(preparedStatement);
            // Execute query or update the query
            ResultSet rs = preparedStatement.executeQuery();

            // process ResultSet
            while (rs.next()) {
                int UserID = rs.getInt("UserID");
                String Password = rs.getString("Password");
                user = new userLogin(UserID, Username, Password);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return user;
    }



    // created exception to get better thrown error
    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof  SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


}

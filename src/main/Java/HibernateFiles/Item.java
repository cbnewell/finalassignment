package HibernateFiles;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "itemdb")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int ItemID;

    @Column(name = "ItemName")
    public String ItemName;
    @Column(name = "ItemType")
    public String ItemType;
    @Column(name = "ItemAvail")
    public String ItemAvail;



    public Item(int ItemID, String ItemName, String ItemType, String ItemAvail) {

        super();
        this.ItemID = ItemID;
        this.ItemName = ItemName;
        this.ItemType = ItemType;
        this.ItemAvail = ItemAvail;
    }

    public Item(String ItemName, String ItemType, String ItemAvail) {

        super();
        this.ItemName = ItemName;
        this.ItemType = ItemType;
        this.ItemAvail = ItemAvail;


    }

    public Item() {

    }

    public int getItemID() {
        return ItemID;
    }

    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String ItemType) {
        this.ItemType = ItemType;
    }

    public String getItemAvail(){return  ItemAvail;}

    public void setItemAvail(String ItemAvail) { this.ItemAvail = ItemAvail;}


    public String toString() { return this.ItemName + " " + this.ItemType + " " + this.ItemAvail;}
}

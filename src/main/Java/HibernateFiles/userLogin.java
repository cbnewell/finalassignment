package HibernateFiles;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "userdb")
public class userLogin implements Serializable {

    @Id
    @GeneratedValue
    public int UserID;
    @Column(name = "Username")
    public String Username;
    @Column(name = "Password")
    public String Password;




    public userLogin(int UserID, String Username, String Password) {
        super();
        this.UserID = UserID;
        this.Username = Username;
        this.Password = Password;
    }

    public userLogin(String Username, String Password) {

        super();
        this.Username = Username;
        this.Password = Password;


    }

    public userLogin() {

    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }


}

package ServletFiles;

import HibernateFiles.Item;
import HibernateFiles.ItemDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/ItemServlet")
public class ItemServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ItemDAO itemDao;

    public void init(ServletConfig config) {
        itemDao = new ItemDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        doGet(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getServletPath();

        switch (action)
        {
        case "/new":
            showNewForm(request,response);
            break;

        case "/insert":
            try {
                insertItem(request,response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            break;

        case "/delete":
            try {
                deleteItem(request,response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            break;

        case "/edit":
            try {
                showItemEditForm(request,response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            break;

        case "/update":
            try {
                updateItems(request, response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            break;

        default:
            try {
                listUser(request,response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            break;
        }

    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("item-form.jsp");
        dispatcher.forward(request,response);
    }
    //insert items
    private void insertItem(HttpServletRequest request, HttpServletResponse response) throws  IOException, SQLException {

        String ItemName = request.getParameter("ItemName");
        String ItemType = request.getParameter("ItemType");
        String ItemAvail = request.getParameter("ItemAvail");

        Item newItem = new Item(ItemName, ItemType, ItemAvail);

            itemDao.insertItems(newItem);
        response.sendRedirect("list");
    }
    // delete item
    private void deleteItem(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        int ItemID = Integer.parseInt(request.getParameter("ItemID"));
        try {
            itemDao.deleteItem(ItemID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("list");
    }
    // edit items
    private void showItemEditForm(HttpServletRequest request, HttpServletResponse response) throws  SQLException {

        int ItemID = Integer.parseInt(request.getParameter("ItemID"));

        Item existingItem;
        try {
            existingItem = itemDao.selectItem(ItemID);
            RequestDispatcher dispatcher = request.getRequestDispatcher("item-form.jsp");
            request.setAttribute("item", existingItem);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // update items
    private void updateItems(HttpServletRequest request, HttpServletResponse response) throws  SQLException, IOException {
        int ItemID = Integer.parseInt(request.getParameter("ItemID"));

        String ItemName = request.getParameter("ItemName");
        String ItemType = request.getParameter("ItemType");
        String ItemAvail = request.getParameter("ItemAvail");

        Item item = new Item(ItemID, ItemName, ItemType, ItemAvail);
            itemDao.updateItem(item);
            response.sendRedirect("list");

    }
    // default

    private void listUser(HttpServletRequest request, HttpServletResponse response) throws SQLException {

        try {
            List<Item> itemList = itemDao.selectAllItems();
            request.setAttribute("itemList", itemList);
            RequestDispatcher dispatcher = request.getRequestDispatcher("item-list.jsp");
            dispatcher.forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

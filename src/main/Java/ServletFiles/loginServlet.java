package ServletFiles;

import HibernateFiles.LoginDAO;
import HibernateFiles.userLogin;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private LoginDAO loginDAO;

    public void init(ServletConfig config) {
        loginDAO = new LoginDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        doGet(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getServletPath();

        switch (action)
        {
            case "/new":
                showNewForm(request,response);
                break;

            default:
                try {
                    selectUser(request,response);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("login-form.jsp");
        dispatcher.forward(request,response);
    }


    // default

    private void selectUser(HttpServletRequest request, HttpServletResponse response) throws SQLException {

        String Username = request.getParameter("Username");

        userLogin existingUser;
        try {
            existingUser = loginDAO.selectUserByName(Username);
            RequestDispatcher dispatcher = request.getRequestDispatcher("user-form.jsp");
            request.setAttribute("user", existingUser);
            // set up authentication class

            dispatcher.forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
